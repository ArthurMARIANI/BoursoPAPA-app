import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs/Observable';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-menubar',
  templateUrl: './menubar.component.html'
})
export class MenuBarComponent implements OnInit {

  @Output() clickBurger: EventEmitter<void> = new EventEmitter<void>();

  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  constructor(private translateService: TranslateService,
              private breakpointObserver: BreakpointObserver) {
  }

  ngOnInit() {
  }

  changeLanguage(language: string): void {
    (async () => {
      await this.translateService.use(language).toPromise();
    })();
  }
}
