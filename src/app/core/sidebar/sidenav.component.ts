import { Component } from '@angular/core';
import { IMenu, MENUS } from './menu-element';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SideNavComponent {

  public menus: IMenu[] = MENUS;

  constructor() {
  }

}
