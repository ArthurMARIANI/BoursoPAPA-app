import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs/Rx';

@Injectable()
export class ProgressBarService {
  public updateProgressBar$: BehaviorSubject<string> = new BehaviorSubject<string>('none');

  private requestsRunning = 0;

  constructor() {
  }

  public list(): number {
    return this.requestsRunning;
  }

  public increase(): void {
    if (++this.requestsRunning === 1) {
      this.updateProgressBar$.next('query');
    }
  }

  public decrease(): void {
    if (this.requestsRunning > 0 && --this.requestsRunning === 0 ) {
        this.updateProgressBar$.next('none');
    }
  }
}
