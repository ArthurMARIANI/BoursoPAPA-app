import { TranslateService } from '@ngx-translate/core';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import { ProgressBarService } from './core/shared/progress-bar.service';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  public progressBarMode = 'none';
  public Auth = null;
  public ifShowSideMenu = false;

  public isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(
    Breakpoints.Handset
  );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private translateService: TranslateService,
    private progressBarService: ProgressBarService,
    private afAuth: AngularFireAuth,
  ) {
    this.translateService.setDefaultLang(environment.defaultLanguage);
    this.progressBarService.updateProgressBar$
      .subscribe(mode => this.progressBarMode = mode);

    this.afAuth.authState.subscribe(auth => this.Auth = auth);
    this.isHandset.subscribe(({matches}) => this.ifShowSideMenu = !matches);
  }
}
