import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { APP_CONFIG, AppConfig } from './config/app.config';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/modules/shared.module';
import { CoreModule } from './core/core.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from './app.translate.factory';
import { ProgressBarService } from './core/shared/progress-bar.service';
import { ProgressInterceptor } from './shared/interceptors/progress.interceptor';
import { TimingInterceptor } from './shared/interceptors/timing.interceptor';
import { LayoutModule } from '@angular/cdk/layout';
import { TestComponent } from './test/test.component';
import { FirebaseModule } from './shared/modules/firebase/firebase.module';
import { TodoService } from './shared/services/todo.service';
import { AngularFirestore } from 'angularfire2/firestore';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    SharedModule.forRoot(),
    CoreModule,
    AppRoutingModule,
    LayoutModule,
    FirebaseModule,
  ],

  declarations: [AppComponent, TestComponent],
  providers: [
    {provide: APP_CONFIG, useValue: AppConfig},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ProgressInterceptor,
      multi: true,
      deps: [ProgressBarService]
    },
    {provide: HTTP_INTERCEPTORS, useClass: TimingInterceptor, multi: true},
    TodoService,
    AngularFirestore,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
