import { InjectionToken } from '@angular/core';

export let APP_CONFIG = new InjectionToken('app.config');

export interface IAppConfig {
  routes: { error404: string };
  endpoints: {};
  snackBarDuration: number;
  repositoryURL: string;
}

export const AppConfig: IAppConfig = {
  routes: {
    error404: '404'
  },
  endpoints: {},
  snackBarDuration: 3000,
  repositoryURL: ''
};
