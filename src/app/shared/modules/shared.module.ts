import { ModuleWithProviders, NgModule } from '@angular/core';
import { MaterialModule } from './material.module';
import { FirebaseModule } from './firebase/firebase.module';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [MaterialModule, FlexLayoutModule, TranslateModule, FirebaseModule],
  exports: [MaterialModule, FlexLayoutModule, TranslateModule, FirebaseModule]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
    };
  }
}
