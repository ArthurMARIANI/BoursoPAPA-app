import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { switchMap } from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';

/*
 * If there is a token added by firebase-auth
 * Add the token on the header.Authorization for every http request
 */

export class AuthorizationHeaderInterceptor implements HttpInterceptor {
  constructor(private afAuth: AngularFireAuth) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.afAuth.idToken.pipe(
      switchMap((token: string) => {
        if (token) {
          request = request.clone({setHeaders: {Authorization: token}});
        }
        return next.handle(request);
      })
    );
  }
}
