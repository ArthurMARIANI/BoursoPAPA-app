import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Rx';

export interface ITodo {
  task: string;
  isDone: boolean;
}

@Injectable()
export class TodoService {
  constructor(private afs: AngularFirestore) {
  }

  getTodos(): Observable<ITodo[]> {
    return this.afs.collection<ITodo>('todos').valueChanges();
  }
}
