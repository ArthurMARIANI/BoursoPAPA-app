#!/bin/bash
DIR=${PWD}/$( dirname "${BASH_SOURCE[0]}")/
APP_DIR=${DIR}/../

docker run --name boursopapa_dev -p 4200:4200 --rm -it -v ${APP_DIR}:/app boursopapa-dev:latest
