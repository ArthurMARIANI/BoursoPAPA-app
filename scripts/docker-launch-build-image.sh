#!/bin/bash
DIR=${PWD}/$( dirname "${BASH_SOURCE[0]}")/
APP_DIR=${DIR}/../

cd $APP_DIR
docker build --rm -f DockerFile-dev -t boursopapa-dev .
./scripts/docker-launch-image-only.sh
