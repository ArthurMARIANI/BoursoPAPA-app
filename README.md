## This is BoursoPapa App

## launch With Docker **DEVELOPEMENT**
The image is a a npm serve to port 4200
# 1. Installation 
First install docker on the system: https://docs.docker.com/install/
# 2. Launch the bash script to build/launch docker container
If it's the first time, you must build the image, then launch the container according to the image  
Otherwise, you can launch direct the container,  
But if you add new package on package.json, you must rebuild the image  
There are 2 scripts on scripts/ folder, for launch the docker operation.

- Build a docker image and launch a container : ./scripts/docker-launch-build-image.sh
- launch the container only : ./scripts/docker-launch-image-only.sh

Both script will launch the angular app on localhost:4200/

## launch With Docker **PRODUCTION**
The image generate is a build of the angular app, and ngix will serve it on its port 80
# 1. Installation 
First install docker on the system: https://docs.docker.com/install/
# 2. Build the images
On the root folder containing the DockerFile  
      
    docker build --rm -f DockerFile -t boursopapa-app:latest .

You can change boursopapa-app:latest to {name-of-app}:{tag}

# 3. Launch the images into a container

    docker run --rm -d -p 8080:80 boursopapa-app:latest

Go to localhost:8080/ to see.


# 4. Optionnal: save and load the docker image
save to a tar file :

    docker save -o myapp.tar boursopapa-app:latest

load from a tar file: 

    docker load < myapp.tar


# TIPS
use docker extension for vscode or your favorite IDE.
